#!/usr/bin/env python
#
from distutils.core import Command, setup
from unittest import TextTestRunner, TestLoader
from glob import glob
from os.path import splitext, basename, join as pjoin, walk
import os
import sys
# Importing setuptools adds some features like "setup.py develop", but
# it's optional so swallow the error if it's not there.
try:
    import setuptools
except ImportError:
    pass

# The code for the Test and Clean commands came from
# http://da44en.wordpress.com/2002/11/22/using-distutils/
class TestCommand(Command):
    user_options = [ ]

    def initialize_options(self):
        self._dir = os.getcwd()

    def finalize_options(self):
        pass

    def run(self):
        '''
        Finds all the tests modules in tests/, and runs them.
        '''
        testfiles = [ ]
        for t in glob(pjoin(self._dir, 'tests', '*.py')):
            if not t.endswith('__init__.py'):
                testfiles.append('.'.join(
                    ['tests', splitext(basename(t))[0]])
                )

        tests = TestLoader().loadTestsFromNames(testfiles)
        t = TextTestRunner(verbosity = 1)
        t.run(tests)


class CleanCommand(Command):
    user_options = [ ]

    def initialize_options(self):
        self._clean_me = [ ]
        for root, dirs, files in os.walk('.'):
            for f in files:
                if f.endswith('.pyc'):
                    self._clean_me.append(pjoin(root, f))

    def finalize_options(self):
        pass

    def run(self):
        for clean_me in self._clean_me:
            try:
                os.unlink(clean_me)
            except:
                pass


kwargs = {}

extensions = []
major, minor = sys.version_info[:2]
version = "2.0git"

if major >= 3:
    import setuptools  # setuptools is required for use_2to3
    kwargs["use_2to3"] = True

setup(
    name="cmdapi",
    version=version,
    packages = ["cmdapi", "cmdapi.test"],
    package_data = {
        },
    ext_modules = extensions,
    author="MichaelGale",
    author_email="gale.michael@gmail.com",
    download_url="https://bitbucket.org/mgale/cmdapi",
    description="""cmdapi is a generic class that turns Linux shell commands into Pytohn objects""",
    cmdclass = { 'test': TestCommand, 'clean': CleanCommand },
    **kwargs
)