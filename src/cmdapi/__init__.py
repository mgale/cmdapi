import os
import sys
import subprocess
import re
from collections import OrderedDict

invalid_chars = re.compile("[^a-zA-Z0-9/]")
opt_shortlong_withargument = re.compile("^ +-([a-zA-Z0-9]+), +--([a-zA-Z0-9-]+)=")
opt_shortlong = re.compile("^ +-([a-zA-Z0-9]+), +--([a-zA-Z0-9-]+) ")
opt_short = re.compile("^ +-([a-zA-Z0-9]+) ")
opt_long_withargument = re.compile("^ +--([a-zA-Z0-9-]+)=")
opt_long = re.compile("^ +--([a-zA-Z0-9-]+) ")

class LinuxCmdOpt(object):
    def __init__(self, option, argument=None, dash_type="-", equals_required=False):
        self.option = option
        self.option_argument = argument
        self.equals_required = False
        self.dash_type = dash_type
        self.help_msg = None

    def set_help(self, msg):
        self.help_msg = msg

    def get_help(self):
        return self.help_msg

    def get_opt(self):

        opt = "%s%s" % (self.dash_type, self.option)

        if self.option_argument:
            if self.equals_required:
                opt = "%s=%s" % (opt, self.option_argument)
            else:
                opt = "%s %s" % (opt, self.option_argument)
        
        return opt

class LinuxCmd(object):
    def __init__(self, cmd):
        self.cmd = self._parse_cmd(cmd)
        self._cmd_opts_order = OrderedDict()
        self._getopts_for_cmd()
        self._cmd_pos_args = []

    def _parse_cmd(self, cmd):
        if invalid_chars.search(cmd):
            raise Exception("Non-ascii characters in command: %s"%(cmd))

        return cmd

    def _getopts_for_cmd(self):
        output = subprocess.check_output(['man', '--no-hyphenation',
            '--no-justification',
            self.cmd])

        docstring_plus = "\n\n"
        docstring_plus += "#### Python command arguments conversion information ####"
        docstring_plus += "\n"

        output_list = output.split("\n\n")
        for possible_opts in output_list:

            opt_name = None
            opt_req_arguemnt = False
            opt_dash = "-"

            if opt_shortlong_withargument.match(possible_opts):
                opt_name = opt_shortlong_withargument.match(possible_opts).group(2)
                opt_req_arguemnt = True
                opt_dash = "--"

            if opt_shortlong.match(possible_opts):
                opt_name = opt_shortlong.match(possible_opts).group(2)
                opt_dash = "--"

            if opt_short.match(possible_opts):
                opt_name = opt_short.match(possible_opts).group(1)

            if opt_long_withargument.match(possible_opts):
                opt_name = opt_long_withargument.match(possible_opts).group(1)
                opt_req_arguemnt = True
                opt_dash = "--"

            if opt_long.match(possible_opts):
                opt_name = opt_long.match(possible_opts).group(1)
                opt_dash = "--"

            if opt_name:
                docstring_plus += "#Instance Attr: %s\n" % (opt_name)
                docstring_plus += "#%s \n" % (possible_opts)
                docstring_plus += "\n\n"
                self._cmd_opts_order[opt_name] = opt_dash
                object.__setattr__(self, "%s" % (opt_name), None)

        self.__init__.__func__.__doc__ = docstring_plus
        self.get_cmd_manpage.__func__.__doc__ = output

    def _create_cmd_list(self):
        cmd_list = [self.cmd]
        for cmd_opt, cmd_dash in self._cmd_opts_order.iteritems():
            opt = getattr(self, cmd_opt)
            if isinstance(opt, bool) and opt:
                cmd_list.append("%s%s" % (
                    cmd_dash,
                    cmd_opt
                    )
                )

            if isinstance(opt, str):
                cmd_list.append("%s%s=%s" % (
                    cmd_dash,
                    cmd_opt,
                    opt
                    )
                )

        return cmd_list

    def execute(self, shell=False):
        """
        Runs the command and returns the output as a byte string
        """
        output = subprocess.check_output(self._create_cmd_list(), shell=shell)
        return output

    def show_execute(self):
        """
        Returns the command that would have been run
        """
        return ''.join(self._create_cmd_list())

    def add_arg(self, arg):
        self._cmd_pos_args.append(arg)

    def remove_arg(self, arg):
        self._cmd_pos_args.remove(arg)

    def get_cmd_manpage(self):
        return self.get_cmd_manpage.__func__.__doc__




    
