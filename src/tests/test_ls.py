import unittest
import cmdapi

class lsTest(unittest.TestCase):
    def setUp(self):
        self.cmd = cmdapi.LinuxCmd('ls')

    def test_cmd_default(self):
        self.assertEqual(self.cmd._create_cmd_list(), ['ls'])

    def test_boolarg(self):
        self.cmd.l = True
        cmd_list = self.cmd._create_cmd_list()
        self.assertEqual(cmd_list, ['ls','-l'])

    def test_passarg(self):
        self.cmd.block-size='M'
        cmd_list = self.cmd._create_cmd_list()
        self.assertEqual(cmd_list, ['ls','--block-size=M'])

if __name__ == '__main__':
    unittest.main()